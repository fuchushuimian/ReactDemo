import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
    return (
        <div className="App">
            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo"/>
                <h1>Welcome To React!</h1>
            </header>
            <h2 class="App-link">Hello World!</h2>
        </div>
    );
}

export default App;
