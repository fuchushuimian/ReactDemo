import React, {Component} from "react";
import PostItem from "./PostItem";

class PostList extends Component {
    render() {
        const data = [{"title": "React", "author": "张三", "date": "2018-10-10"}];
        return (
            <div>
                帖子列表:
                <ul>
                    {data.map(item => (
                        <PostItem author={item.author} title={item.title} date={item.date} key={item}/>
                    ))}
                </ul>
            </div>
        );
    }
}

export default PostList;

