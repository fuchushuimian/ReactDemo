import React from 'react'
import {
    render
} from 'react-dom'
import Menu from './components/Menu'
import data from './data/recipes'
import './style/Menu.css'
window.React = React
console.info(data);
render(<Menu recipes={data}/>, document.getElementById('app'));