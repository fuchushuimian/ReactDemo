var webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
    entry: __dirname + "/index.js",
    output: {
        path: __dirname + "/dist/assets",
        filename: "bundle.js",
        sourceMapFilename: "bundle.map"
    },
    devtool: '#source-map',
    module: {
        rules: [{
            test: /\.js$/,
            exclude: /(node_modules)/,
            use: [{
                loader: "babel-loader",
                options: {
                    presets: [
                        "env", "react",
                    ]
                }
            }]
        }, {
            test: /\.css$/,
            exclude: /(node_modules)/,
            use: ['style-loader', 'css-loader']
        }]
    },
    optimization: { //与entry同级
        minimizer: [
            new UglifyJsPlugin({
                uglifyOptions: {
                    compress: false,
                    mangle: true,
                    output: {
                        comments: false,
                    },
                },
                sourceMap: true,
            })
        ]
    }
}