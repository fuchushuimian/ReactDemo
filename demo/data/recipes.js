const data = [{
    "name": "Baked Salmon",
    "ingredients": [{
        "name": "Salmon",
        "amount": 1,
        "measurement": "l 1b"
    }, {
        "name": "Pine Nuts",
        "amount": 2,
        "measurement": "l 1b"
    }],
    "steps": ["step1 .", "step2  blablabla ."]
}, {
    "name": "Fish Tacos",
    "ingredients": [{
        "name": "Salmon",
        "amount": 1,
        "measurement": "l 1b"
    }, {
        "name": "Pine Nuts",
        "amount": 2,
        "measurement": "l 1b"
    }],
    "steps": ["step1 .", "step2  blablabla ."]
}];

export default data;