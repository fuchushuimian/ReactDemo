import React, {Component} from "react";
import PropTypes from 'prop-types';

class PostItem extends Component {


    handleClick = () => {
        this.props.onVote(this.props.post.id);
    };

    render() {
        return (
            <li>
                <div>
                    <div>{this.props.post.title}</div>
                    <div>创建人：<span>{this.props.post.author}</span></div>
                    <div>创建时间：<span>{this.props.post.date}</span></div>
                    <div>
                        <button onClick={this.handleClick}>点赞</button>
                        &nbsp;
                        <span>
                            {this.props.post.vote}
                        </span>
                    </div>
                </div>
            </li>
        );
    }
}

PostItem.propTypes = {
    post: PropTypes.shape({
            id: PropTypes.number,
            title: PropTypes.string,
            author: PropTypes.string,
            date: PropTypes.string,
            vote: PropTypes.number,
        }.isRequired,
    ),
    onVote: PropTypes.func.isRequired,
};


export default PostItem;

